<?php
namespace Zerustech\HelloWorld;

class Foo
{
    public function hello()
    {
        echo "hello\n";
    }

    public function world()
    {
        echo "world\n";
    }
}
